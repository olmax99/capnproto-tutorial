module capnproto-tutorial

go 1.18

require (
	capnproto.org/go/capnp/v3 v3.0.0-alpha.7 // indirect
	golang.org/x/sync v0.0.0-20220923202941-7f9b1623fab7 // indirect
)
